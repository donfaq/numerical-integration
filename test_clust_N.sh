    #!/bin/bash
    for b in $(seq 0 5)
    do
        for m in r g t s
        do
            for t in $(seq 0 20)
            do
                /usr/bin/time -o res_clust_N.txt -a -f "%C %E %P" mpiexec -nq 2 python main.py $m 0 $b 1000000 0.000001 >> res_clust_N.txt
            done
        done
    done
