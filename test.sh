    #!/bin/bash
    for n in 100 1000 10000 100000 1000000 10000000
    do
        for i in $(seq 1 32)
        do
            for m in r g t s
            do
                for t in $(seq 0 5)
                do
                    /usr/bin/time -o res_server.txt -a -f "%C %E %P" mpiexec -qn $i python main.py $m 0 1 $n >> res_server.txt
                done
            done
        done
    done
 
