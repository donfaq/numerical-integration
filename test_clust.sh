    #!/bin/bash
    for n in 100 1000 10000 100000 1000000 10000000
    do
        for i in $(seq 1 15)
        do
            for m in r g t s
            do
                for t in $(seq 0 20)
                do
                    /usr/bin/time -o res_clust.txt -a -f "%C %E %P" mpirun.mpich2 -n $i -host master,node001 python main.py $m 0 1 $n >> res_clust.txt
                done
            done
        done
    done