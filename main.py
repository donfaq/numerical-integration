import sys

from mpi4py import MPI
from numpy import arange, linspace, sqrt, zeros


def f(x):
    return 4 / (1 + x**2)


def trapezoidal(a, b, n):
    integral = -(f(a) + f(b)) / 2
    for x in linspace(a, b, n + 1):
        integral = integral + f(x)
    integral = integral * (b - a) / n
    return integral


def riemann_sum(a, b, n):
    summ = 0
    space = linspace(a, b, n)
    x_prev = space[0]
    for x in space[1:]:
        summ = summ + f(x) * (x - x_prev)
        x_prev = x
    return summ


def simpson(a, b, n):
    h = (b - a) / n
    sum1 = 0
    for i in arange(1, n / 2 + 1):
        sum1 += f(a + (2 * i - 1) * h)
    sum1 *= 4
    sum2 = 0
    for i in arange(1, n / 2):
        sum2 += f(a + 2 * i * h)
    sum2 *= 2
    approx = (b - a) / (3.0 * n) * (f(a) + f(b) + sum1 + sum2)
    return approx

def gauss(a, b, n):
    h = (b - a) / n
    h_2 = h / 2
    h_3 = h / sqrt(3)
    h_half = 0.5 * h
    integral = 0
    for i in arange(1, n):
        x_12 = a + i * h - h_half
        integral = integral + (f(x_12 - h_3) + f(x_12 + h_3)) * h_2
    return integral

METHODS = {'r': riemann_sum, 't': trapezoidal, 's': simpson, 'g': gauss}


def main():
    method = str(sys.argv[1])
    method = METHODS.get(method, None)
    assert method is not None, 'Bad method input'
    a = float(sys.argv[2])
    b = float(sys.argv[3])
    n = float(sys.argv[4])

    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    if len(sys.argv) > 5:
        h = float(sys.argv[5])
        n = (b - a) / h
    else:
        h = (b - a) / n

    local_n = n / size
    local_a = a + rank * local_n * h
    local_b = local_a + local_n * h

    integral = zeros(1)
    recv_buffer = zeros(1)

    integral[0] = method(local_a, local_b, local_n)

    if rank == 0:
        total = integral[0]
        for i in range(1, size):
            comm.Recv(recv_buffer, MPI.ANY_SOURCE)
            total += recv_buffer[0]
    else:
        comm.Send(integral, 0)

    if comm.rank == 0:
        sys.stdout.write('{0} '.format(total.__repr__()))


if __name__ == '__main__':
    main()
